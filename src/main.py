from flask import Flask
import url
import redis

app = Flask(__name__)
r = redis.Redis(host='redis-service', port=6379, db=0)
# connect to Redis server

@app.route('/')
def fun():
    return "it works"

@app.route('/<youtube_url>')
def relevance(youtube_url):
    end_url="www.youtube.com/watch?v="+youtube_url
    for word in url.main(end_url):
        if r.exists(word.upper()):
            return "true"
    return "false"



if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port =80)
