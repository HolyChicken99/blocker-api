import os
import sys
import googleapiclient.discovery


def main(url):
    if "youtube.com" in url:
        video_id = url.split("v=", 1)[1]
        if '&' in video_id:
            video_id = video_id.rpartition('&')[0]

        os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

        api_service_name = "youtube"
        api_version = "v3"

        youtube = googleapiclient.discovery.build(
            api_service_name, api_version, developerKey="AIzaSyAXDGP_ncNCRwdZ-2zeQYUhy1C5idHjAOQ")

        request = youtube.videos().list(
            part="snippet",
            id=video_id
        )
        video_title = request.execute()['items'][0]['snippet']['title']
        list_title = video_title.split()
        return list_title

# if __name__=='__main__':
#     main("https://www.youtube.com/watch?v=_hvX9MqgaIA&t=5s%22")
