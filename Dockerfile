FROM alpine
RUN apk update \
 && apk add git \
 && apk add py3-pip \
 && git clone https://gitlab.com/HolyChicken99/blocker-api.git \
 && cd ./blocker-api/src/ && ls && pip install -r req.txt

EXPOSE 80/tcp
CMD python3 ./blocker-api/src/main.py
